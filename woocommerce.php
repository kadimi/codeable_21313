<?php 
	get_header();
	
	if( is_single() && 'yes' == get_option('single_product_header','yes') ){
		get_template_part('woocommerce/content','product-header'); 
	} elseif(!( is_single() )) {
		get_template_part('woocommerce/content','archive-header'); 
	}
?>

	<section class="article-single dark-wrapper">
		<div class="<?php echo is_single() ? 'container' : 'container-fluid no-padding' ?>">
			<div class="col-sm-12 <?php echo is_single() ? '' : 'no-padding' ?>">
				<?php
					woocommerce_content();	
					wp_link_pages();
				?>
			</div>
		</div><!--end of container-->	
	</section>

<?php get_footer();
