<?php 

// Display 12 products per page
add_filter( 'loop_shop_per_page', create_function( '$x', 'return 12;' ), 20 );

// Use 3 product columns
add_filter( 'loop_shop_columns', create_function( '$x', 'return 3;' ), 20 );
